The purpose of this excersise is evaluate your ability to get around and perform basic tasks in a linux environment.  There will be 2 sections: 1) Basic linux administration, and 2) Scripting in bash.  Once you have completed the project email Rob - rlyon@kochava.com and we will begin the review.

#### Things to remember

* Keep a journal of what you are doing through the installation process and scripting.  As you are doing this on your own, knowing some of the issues that you encountered and how you solved them.
* If you run into issues, you can always contact Rob to ask questions and get hints on how to proceed.  Asking the right questions is just as important as knowing the answer right away, especially in a cutting edge tech company.
* If you find that there are errors or omissions in the test, feel free to reach out for clarification.
* Your repo that you set up below will serve as a place you will add your scripts and journal, not only so you have a place to store things, but so we can follow along with what your are doing, so be sure to push regularly.  I'll be checking in on a regular basis to gauge your progress.

#### Setup

1. Create an account at [gitlab.com](https://gitlab.com) if you do not already have one and then create a new private repository to store your work.  Share this private repository with `@kochavaops` so we can follow your progress.
2. Your welcome email should have contained login information for a Digital Ocean dropplet that you will use to deploy your application.  Make sure that it is available and that you can log in.

## Basic linux administration

1) Log into the instance that has been provided to you.  We will be working on an Ubuntu 18.04 system.  Most distributions are the same at a core level but management of those system can vary in terms of location of service files, package management, and service interaction.
2) We will be setting up a simple web server under normal conditions we generally run with the default users that are created as part of the webserver installation process - but we are not going to do that here.
    * Set up a new group called `webserver` that has a group id of `400`
    * The first thing that we want to do is set up a new user named: `webserver`
        * The name of the user will be `webserver`
        * It will belong to the `webserver` group
        * It's uid will be `400`
        * This should also be a system user that has no login capabilities.  System users are users that are only used to run services, daemons, or software.
3) Once you have the user set up, install and configure nginx from the 'official' nginx PPA (Personal Package Archive).  Install the mainline version (currently at 1.15.8-0)
4) Create a directory at /var/www/testing and change the ownership to the webserver user.
5) Configure nginx to start as the webserver user and group that you created and set up it's root to point at the directory that you created in step #4
6) Test out your install by creating a barebones index.html file that contains `hello world`.
7) If everything is working correctly, you should be able to point your browser to the ip address of your instance and see `hello world`

## Scripting

1) Upload and uncompress/unarchive the log file that was sent with this email to your instance.
2) You will be creating 3 bash shell (2 required and 1 bonus) scripts to parse the contents of this log file in different formats.  The file is standard syslog format and you will be able to grab the column identifiers from the RFC.  The scripts and the directions are as found below.
3) Run the scripts and redirect the output to files in the web root that are named after the script name replacing the `.sh` extention with `.txt`
4) If everything works correctly then you should be able to point your browser all of the urls:
    * http://<IP>/summarize_entries.txt
    * http://<IP>/summarize_by_host.txt
    * http://<IP>/summarize_hour.txt

#### summarize_entries.sh

Sum the number of entries per host and sum the number of enteries per appname. If there is a process id attached to the app name (i.e. appname[1234]), remove it.  This should output two lists.  Your output should look something like this:

```
HOST        COUNT
host1       1234
host2       123
host3       13

APP_NAME    COUNT
app1        1234
app2        234
app3        34
app4        4
```

#### summarize_by_host.sh

Modify the script to break down the entries on a per host basis into the count of applications and sort them by app name frequency.  Output should look something like this:

```
host1:
[APPNAME] [COUNT]
...
[APPNAME] [COUNT]

host2:
[APPNAME] [COUNT]
...
[APPNAME] [COUNT]

host3:
[APPNAME] [COUNT]
...
[APPNAME] [COUNT]
```

#### (Bonus) summarize_by_hour.sh

Add a new list that provides a frequency count by hour with a decending sorted comma seperated list of the top 5 app names after the count.

```
[0-23] [COUNT] [APP_NAME, APP_NAME, APP_NAME, APP_NAME, APP_NAME]